<?php
namespace FMT;

/**
  * Documentacion completa de Redis para PHP: https://github.com/phpredis/phpredis
*/

class RedisCache {
    static private $_redis_instance     = null;
    static private $_self_instance      = [];
    private $app_prefix                 = null;
    static private $DEFAULT_TTL_SECONDS = 600; // 10 minutos

    /**
     * Obtiene o genera una instancia de RedisCache.
     * Si, app_prefix es pasado, permite acceder a datos de otras aplicaciones.
     *
     * @param string $app_prefix
     * @return void
     */
    static public function getInstance($app_prefix=null){
        $config = static::getConfig();
        $app_prefix = !is_string($app_prefix) ? $config->app_prefix : $app_prefix;
        if (!isset(static::$_self_instance[$app_prefix])) {
            static::$_self_instance[$app_prefix]    = new static($app_prefix);
        }
        return static::$_self_instance[$app_prefix];
    }

    /**
     * Se invoca al hacer un new RedisCache() o al usar la forma RedisCache::getInstance();
     */
    final protected function __construct($app_prefix=null) {
        $config             = static::getConfig();
        $app_prefix         = !is_string($app_prefix) ? $config->app_prefix : $app_prefix;
        $this->app_prefix   = $app_prefix;

        $this->redisInstance();
    }
    final public function __destruct() {
        foreach (array_keys(static::$_self_instance) as $v) {
            $i = static::$_self_instance[$v]->redisInstance();
            $i->close();
        }
    }
    static public function init($app_prefix=null){
        return static::getInstance($app_prefix);
    }

    /**
     * Genera la configuracion para conectar contra Redis
     * @return \stdClass
     */
    static private function getConfig(){
        static $return = null;
        static $_db_name = null;
        
        if($return !== null) {
            return $return;
        }

        $return = (object)[
          'host'        => !empty($_ENV['REDIS_HOST']) ? $_ENV['REDIS_HOST'] : '',
          'port'        => !empty($_ENV['REDIS_PORT']) ? $_ENV['REDIS_PORT'] : 6379,
          'user'        => !empty($_ENV['REDIS_USER']) ? $_ENV['REDIS_USER'] : '',
          'pass'        => !empty($_ENV['REDIS_PASS']) ? $_ENV['REDIS_PASS'] : '',
          'db_index'    => !empty($_ENV['REDIS_DB_INDEX']) ? $_ENV['REDIS_DB_INDEX'] : '1',
          'app_prefix'  => !empty($_ENV['REDIS_APP_PREFIX']) ? $_ENV['REDIS_APP_PREFIX'] : '',
        ];

        switch (true) {
            case empty($return->host):
                throw new \Exception("host para redis no fue configurado", 1);
                break;
            case empty($return->pass):
                throw new \Exception("pass para redis no fue configurado", 1);
                break;
            case empty($return->app_prefix):
                throw new \Exception("app_prefix para redis no fue configurado", 1);
                break;
        }
        
        return $return;
    }

    /**
     * Crea o devuelve una instancia de Redis.
     * Es donde se setean los datos de conexion y reglas varias
     *
     * @return \Redis
     */
    final public function redisInstance() {
        if($this->app_prefix == null){
            throw new \Exception("Ningun string valido fue pasado para obtener la instancia de Redis", 1);
        }

        if(static::$_redis_instance !== null && static::$_redis_instance instanceof \Redis){
            return static::$_redis_instance;
        }

        $config = static::getConfig();
        static::$_redis_instance  = new \Redis();
        static::$_redis_instance->connect($config->host, $config->port);
        static::$_redis_instance->auth($config->pass);
        static::$_redis_instance->select((int)$config->db_index);
        
        static::$_redis_instance->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE); // Don't serialize data
        static::$_redis_instance->setOption(\Redis::OPT_PREFIX, $this->app_prefix.':');
        return static::$_redis_instance;
    }   
    
    /**
     * Setea una llave => valor en redis
     *
     * @param string $key - Clave con la que almacenar el valor
     * @param string $val - Valor para almacenar
     * @param int $ttl - Tiempo de expiracion expresado en Segundos
     * @return RedisCache
     */
    public function set($key=null, $val=null, $ttl=null){
        if(!(is_string($val) || is_numeric($val) || is_bool($val) || is_null($val))){
            throw new \Exception("El valor para set debe ser String, se recibió: ". gettype($val) , 1);
        }
        if(!(is_numeric((int)$val) || is_null($val))){
            throw new \Exception("El valor para ttl debe ser numerico, se recibió: ". gettype($val) , 1);
        }
        // nx = set if non exists
        // xx = set if exists 
        $ttl    = $ttl == null ? static::$DEFAULT_TTL_SECONDS : $ttl;
        $ttl    = !is_numeric($ttl) ? [] : ['ex'=>$ttl];
        $i  = $this->redisInstance();
        $i->set($key, $val, $ttl);
        return $this;
    }

    /**
     * Devuelve un valor almacenado en Redis tal cual fue guardado.
     *
     * @param string $key - Clave con la que obtener el valor almacenado
     * @return string|bool|int
     */
    public function get($key=null){
        $i  = $this->redisInstance();
        return $i->get($key);
    }
    
    
    /**
     * Setea una llave => valor en redis para tipo de alamacenamiento en forma de Lista con indice numerico
     *
     * @param string $key - Clave con la que almacenar el valor
     * @param string $val - Valor para almacenar
     * @return RedisCache
     */
    public function addList($key=null, $val=null){
        if(!(is_string($val) || is_numeric($val) || is_bool($val) || is_null($val))){
            throw new \Exception("El valor para addList debe ser String, se recibió: ". gettype($val) , 1);
        }
        $i  = $this->redisInstance();
        $i->rPush($key, $val);
        return $this;
    }

    /**
     * Permite leer de una lista en forma de FIFO ( First Input First Output, El primer dato en entrar, es el ultimo en salir)
     *
     * @param string $key
     * @return string|false
     */
    public function getFifo($key=null){
        $i  = $this->redisInstance();
        return $i->lPop($key);
    }
    

    /**
     * Permite leer de una lista en forma de LIFO ( Last Input First Output, El ultimo dato en entrar, es el primero en salir)
     *
     * @param string $key
     * @return string|false
     */
    public function getLifo($key=null){
        $i  = $this->redisInstance();
        return $i->rPop($key);
    }
}