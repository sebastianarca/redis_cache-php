<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');
define('BASE_PATH', realpath(__DIR__).'/..');
require_once BASE_PATH . '/vendor/autoload.php';
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
// Carga en este orden .env, .env.local, and .env.$APP_ENV.local or .env.$APP_ENV
$dotenv->loadEnv(BASE_PATH.'/.env', null, '');
