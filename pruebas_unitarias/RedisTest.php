<?php
/**
 * Para ejecutar estas pruebas unitarias se debe hacer de la siguiente manera:
 * vendor/bin/phpunit vendor/fmt/redis_cache/pruebas_unitarias/RedisTest.php
 */

use FMT\RedisCache;
class RedisTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Crea un prefijo de claves para las pruebas
     *
     * @var string
     */
    static private $PREFIX_UNIT = 'pruebasunitarias:';
    
    /**
     * Genera valores para manejar las pruebas de forma consistente.
     *
     * @return \stdClass
     */
    static private function createValues(){
        $return = [
            'val1'  => 'val5',
            'val2' => 'val6',
            'fifo'  => [
                '11',
                '22',
                '33',
                '44',
                '55',
                '66',
                '77',
                '88',
                '99',
            ]
        ];
        $return['lifo'] = array_reverse($return['fifo']);
        return (object)$return;
    }

    /**
     * Almacena valores
     *
     * @return void
     */
	public function testGuardarValores(){
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $redis->set(static::$PREFIX_UNIT.'val1', $testVal->val1);
        $redis->set(static::$PREFIX_UNIT.'val2', $testVal->val2);
        
        $this->assertTrue(is_numeric($redis->redisInstance()->lastSave()));
    }

	/**
     * Lee valores almacenados
     *
     * @depends testGuardarValores
     */
    public function testLeerValores(){
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $val1   = $redis->get(static::$PREFIX_UNIT.'val1');
        $val2   = $redis->get(static::$PREFIX_UNIT.'val2');
        
        $this->assertTrue($val1 == $testVal->val1);
        $this->assertTrue($val2 == $testVal->val2);
    }
    
    /**
     * Guarda valores con un tiempo de expiracion de 2 segundos
     *
     * @return void
     */
	public function testGuardarValoresYExpirar(){
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $redis->set(static::$PREFIX_UNIT.'expirar_val1', $testVal->val1, 2);
        $redis->set(static::$PREFIX_UNIT.'expirar_val2', $testVal->val2, 2);
        
        $this->assertTrue(is_numeric($redis->redisInstance()->lastSave()));
    }
	/**
     * Busca valores que debenrian estar expirados y comprueba su resultado
     *
     * @depends testGuardarValoresYExpirar
     */
    public function testLeerValoresExpirados(){
        sleep(3);
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $val1   = $redis->get('expirar_val1');
        $val2   = $redis->get('expirar_val2');
        
        $this->assertTrue($val1 != $testVal->val1);
        $this->assertTrue($val2 != $testVal->val2);
        $this->assertTrue($val1 === false);
        $this->assertTrue($val2 === false);
    }

    
    /**
     * Prueba para manejo de instancias, consistencia entre metodos 
     *
     * @return void
     */
	public function testTestInstanceManager(){       
        $b1 = RedisCache::init();
        $b2 = RedisCache::getInstance();
        
        $c1 = RedisCache::init('qa_sigarhu2');
        $c2 = RedisCache::getInstance('qa_sigarhu2');
        
        $this->assertTrue($b1 instanceof RedisCache);
        $this->assertTrue($b1 === $b2);
        
        $this->assertTrue($b1 !== $c1);
        $this->assertTrue($b1 !== $c2);
    }

    /**
     * Guarda valores en una lista para leerlos en forma de FIFO
     *
     * @return void
     */
    public function testGuardarFifo(){
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        foreach ($testVal->fifo as $value) {
            $redis->addList(static::$PREFIX_UNIT.'fifo', $value);
        }
        $this->assertTrue(is_numeric($redis->redisInstance()->lastSave()));
    }
    /**
     * Probar leer los valores de una lista en forma de FIFO
     *
     * @depends testGuardarFifo
     */
    public function testLeerFifo(){
        sleep(2);
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $fifoValues = [];
        while ($value = $redis->getFifo(static::$PREFIX_UNIT.'fifo')) {
            $fifoValues[]   = $value;
        }
        $this->assertTrue($testVal->fifo === $fifoValues);
    }
    
    
    /**
     * Guarda valores en una lista para leerlos en forma de LIFO
     *
     * @return void
     */
    public function testGuardarLista2(){
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        foreach ($testVal->fifo as $value) {
            $redis->addList(static::$PREFIX_UNIT.'lifo', $value);
        }
        $this->assertTrue(is_numeric($redis->redisInstance()->lastSave()));
    }
    /**
     * Probar leer los valores de una lista en forma de LIFO
     *
     * @depends testGuardarLista2
     */
    public function testLeerLifo(){
        sleep(2);
        $testVal= static::createValues();
		$redis  = RedisCache::getInstance();

        $lifoValues = [];
        while ($value = $redis->getLifo(static::$PREFIX_UNIT.'lifo')) {
            $lifoValues[]   = $value;
        }
        $this->assertTrue($testVal->lifo === $lifoValues);
    }
}