# Redis Cache

Implementacion de la libreria Redis para PHP, ajustado para el uso amigable y reutilizacion de codigo.

## Requisitos

Para Centos 7 o superior el paquete requerido es **php74-php-pecl-redis5**  que debe estar disponible en el entorno donde se ejecute la aplicacion PHP.

## Configuracion

La configuracion se puede hacer mediante archivo con variables de entorno. Archivos de uso disponibles en **./.env**, **./.env.dev**, **./.env.testing**
```
REDIS_HOST='localhost'
REDIS_PORT=6379
REDIS_PASS='1234'
REDIS_APP_PREFIX=testing_rediscache
REDIS_DB_INDEX=1
```


Tambien se pueden hacer mediante el archivo **config/redis.php**


En caso de existir un nombre para la base de datos alojado en **config/database.php** con las llaves **['database']['database']** y que app_prefix este en vacio, se usara el primer valor.


Toda la configuracion se realiza de forma interna en **RedisCache::getConfig()**


Los datos que sean necesarios y no esten configurados, van a generar que tire una excepcion, para facilitar las lectura en logs.


## Referencias externas

Toda la documentacion oficial de la libreria esta disponible en: https://github.com/phpredis/phpredis

## Pruebas unitarias

Las pruebas unitarias se pueden ejecutar al clonar este repositorio ejecutando:
 
 - `./vendor/bin/phpunit`

O bien se pueden ejecutar dentro de un proyecto al tenerlo instalado como dependencia de esta forma:

 - `vendor/bin/phpunit vendor/fmt/redis_cache/pruebas_unitarias/RedisTest.php`


## Formas de Uso

En general todas las formas de uso, la mejor forma para entender su uso es revisando las pruebas unitarias disponibles, de todas formas aqui se deja un detalle general.


```php
// Generar o acceder a una instancia
// Si no es pasado ningun parametro se usa el de la configuracion, caso contrario accede o crea la instancia para el nombre dado
$redis  = RedisCache::getInstance('fqdn_fantastico'); 

// Almacena un valor para una llave dada, si el valor existe lo reemplaza.
// La palabra 'modulo:' no es necesaria, pero si muy util para agrupar llaves en conceptos comunes. Puede ser cualquier string
// La palabra 'llave', puede ser cualquier string y sirve como nombre de variable
// El valor puede ser de tipo: string, numerico, booleano o null
// 600 es el valor por defecto si no se pasa este parametro, y el tiempo de vida (TTL) que tiene es valor, 600 segundos es por defecto en caso de no estar seteado
$redis->set('modulo:llave', 'valor', 600);

// Devuelve el valor tal cual fue guardado con ->set o false si el valor no existe.
$redis->get('modulo:llave');

// Igual que ->get, solo que al agregar mas valores no son reemplazados, si no que se almacenan en una especie de array de Redis (listas)
$redis->addList('modulo:fifo', 'valor1');
$redis->addList('modulo:fifo', 'valor2');

// Obtiene el primer valor que fue guardado en la lista, o false en caso de estar vacia la lista.
// Cada vez que obtiene el valor lo borra de la lista.
$redis->getFifo('modulo:fifo');

// Obtiene el ultimo valor que fue guardado en la lista, o false en caso de estar vacia la lista.
// Cada vez que obtiene el valor lo borra de la lista.
$redis->getLifo('modulo:fifo');
```


## Recomendaciones

Redis tiene muchas formas de uso. Todas estan oriendas a permitir manejas mensajes cacheados y combinados entre distintas aplicaciones.



Algo muy importante a la hora de trabajar con esta tecnologia, es mantener documentacion de los tipos de datos que se van a guardar, y de ser posible escribir abstracciones.

En caso de escribir objetos JSON, es importante que mantengan una consistencia y coherencia a la hora de escribir, actualizar y leer los datos. En especial teniendo en cuenta que seria posible escribir datos desde una aplicacion (servicio) y leerlos desde otro distinto.

## Debug

Para debugear los elementos internos de Redis se puede usar la herramienta https://www.npmjs.com/package/redis-commander instalada en modo local.


Se ejecuta como:   
` redis-commander   --redis-host localhost --redis-password CLAVE_REDIS --redis-db 1`   
y se accede por navegador web a:   
`http://127.0.0.1:8081`



Al ejecutar las pruebas unitaria se pueden ver valores similares a los mostrados en las siguientes imagenes:

![Ejemplo valor simple](./doc/redis_cache_valor_simple.png "Ejemplo valor simple")   

![Ejemplo listas-array](./doc/redis_cache_valor_lista.png "Ejemplo listas-array")

Al leer la key con nombre **qa_rediscache:pruebasunitarias:val1** podemos comprender que **qa_rediscache** es la aplicacion/servicio que esta ejecutando el proceso, **pruebasunitarias** es la seccion de dicho servicio donde se estan generando los datos, y por ultimo **val1** es el nombre de la variable donde se almacenan los datos.

Tambien podemos ver el tipo de dato que se guarda que en este caso es String. Y el TTL que en este caso es 590 segundos, por lo tanto despues de ese tiempo la variable **val1** sera eliminada.


## Lista de pendientes
 -  Buscar dentro de una lista
 -  Publish/Subscribe
 -  Transactions (modo en lote)
 -  Serializacion/Desarilizacion de Objetos, JSON y Arrays
 -  Borrar elementos almacenados